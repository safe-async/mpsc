fn main() {
    jive::block_on(async move {
        let (tx, mut rx) = mpsc::channel();

        for i in 0..10 {
            let tx = tx.clone();
            jive::spawn(async move {
                for j in 1..=10 {
                    let tx = tx.clone();
                    jive::spawn(async move {
                        tx.send(i * 10 + j);
                    });
                }
            });
        }

        drop(tx);

        while let Some(val) = rx.recv().await {
            println!("Got {}", val);
        }
    })
}
